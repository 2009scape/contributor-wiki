_**Welcome to the 2009Scape Contributors' Wiki!**_

This will serve a few purposes, but largely aims to help introduce people to the project and give them the tools and information they need to contribute.

[[_TOC_]]

## Getting Started
- [Git Basics](git-basics)
- [IntelliJ Setup](Setup-for-IntelliJ-IDEA-IDE)
- [ContentAPI Introduction](contentapi)
- [Queue Strengths](queue-strengths)

## Tutorials
- [Writing Listeners (Handling Interactions)](https://gitlab.com/2009scape/2009scape/-/wikis/writing-listeners)
- [Converting Plugins to Listeners](Converting-Plugins-To-Listeners)